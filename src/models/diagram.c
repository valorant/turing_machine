#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "utils.h"
#include "models/diagram.h"

static char err[100];
static node *find_node(diagram *diag, char *name);

diagram *init_diagram(char **node_name, int number_of_node)
{
    diagram *diag = (diagram *)malloc(sizeof(diagram));
    diag->nodes = (node **)malloc(sizeof(node *) * number_of_node);
    diag->number_of_node = number_of_node;
    for (size_t i = 0; i < number_of_node; i++)
    {
        diag->nodes[i] = (node *)malloc(sizeof(node));
        diag->nodes[i]->links = NULL;
        strcpy(diag->nodes[i]->name, node_name[i]);
    }
    return diag;
}

void init_start(char *start, diagram *diag)
{
    node *start_node = find_node(diag, start);
    if (start == NULL)
    {
        sprintf(err, "error: node %s not found\n", start);
        error(err);
    }
    diag->start = start_node;
}

void add_link(char *name_src, char *name_tgt, char IN, char OUT,
              enum direction dir, diagram *diag)
{
    node *tgt = NULL, *src = NULL;
    for (size_t i = 0; i < diag->number_of_node &&
                       (src == NULL || tgt == NULL);
         i++)
    {
        if (strcmp(name_src, diag->nodes[i]->name) == 0)
        {
            src = diag->nodes[i];
        }
        if (strcmp(name_tgt, diag->nodes[i]->name) == 0)
        {
            tgt = diag->nodes[i];
        }
    }
    if (src != NULL && tgt != NULL)
    {
        link_t *new_link = (link_t *)malloc(sizeof(link_t));
        new_link->IN = IN;
        new_link->OUT = OUT;
        new_link->node = tgt;
        new_link->next = src->links;
        new_link->dir = dir;
        src->links = new_link;
    }
    else
    {
        sprintf(err, "error: doesn't exists %s or %s\n", name_src, name_tgt);
        error(err);
        return;
    }
}

static node *find_node(diagram *diag, char *name)
{
    for (size_t i = 0; i < diag->number_of_node; i++)
    {
        if (strcmp(diag->nodes[i]->name, name) == 0)
        {
            return diag->nodes[i];
        }
    }
    return NULL;
}

void free_diagram(diagram *diag)
{
    for (size_t i = 0; i < diag->number_of_node; i++)
    {
        link_t *tmp = diag->nodes[i]->links;
        while (tmp != NULL)
        {
            tmp = tmp->next;
            free(diag->nodes[i]->links);
            diag->nodes[i]->links = tmp;
        }

        free(diag->nodes[i]);
    }
    free(diag->nodes);
    free(diag);
}