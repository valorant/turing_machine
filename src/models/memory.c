#include <sys/ioctl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <ncurses.h>

#include "models/memory.h"
#include "view/view_mem.h"

#define DELAY_SHIFT 100000

static int setup;

static void init_band(memory_t *memory);

memory_t *setup_memory()
{
    init_meme_view();

    memory_t *new = (memory_t *)malloc(sizeof(memory_t));
    init_band(new);

    return new;
}

void finish_memory_setup(memory_t *memory)
{
    setup = 0;
    print_memory(memory, 0);
}

static void init_band(memory_t *memory)
{
    setup = 1;
    band_t *tmp = (band_t *)malloc(sizeof(band_t));
    tmp->character = memory->empty;
    tmp->prev = NULL;
    tmp->prev = NULL;
    memory->band = tmp;
}

void go_right(char c, memory_t *memory)
{
    if (memory->band->next == NULL)
    {
        band_t *tmp = (band_t *)malloc(sizeof(band_t));
        tmp->next = NULL;
        tmp->prev = memory->band;
        tmp->character = memory->empty;
        memory->band->next = tmp;
    }
    memory->band->character = (c != -1) ? c : memory->band->character;
    if (!setup)
    {
        for (int i = 0; i > -5; i--)
        {
            print_memory(memory, i);
            usleep(DELAY_SHIFT);
        }
    }
    memory->band = memory->band->next;
    if (!setup)
    {
        print_memory(memory, 0);
    }
}

void go_left(char c, memory_t *memory)
{
    if (memory->band->prev == NULL)
    {
        band_t *tmp = (band_t *)malloc(sizeof(band_t));
        tmp->prev = NULL;
        tmp->next = memory->band;
        tmp->character = memory->empty;
        memory->band->prev = tmp;
    }
    memory->band->character = (c != -1) ? c : memory->band->character;
    if (!setup)
    {
        for (int i = 0; i < 5; i++)
        {
            print_memory(memory, i);
            usleep(DELAY_SHIFT);
        }
    }
    memory->band = memory->band->prev;
    if (!setup)
    {
        print_memory(memory, 0);
    }
}

void free_memory(memory_t *memory)
{
    free_view_mem();
    while (memory->band->prev != NULL)
    {
        memory->band = memory->band->prev;
    }
    while (memory->band->next != NULL)
    {
        memory->band = memory->band->next;
        free(memory->band->prev);
    }
    free(memory->band);
    free(memory);
}