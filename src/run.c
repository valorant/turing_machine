#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <ncurses.h>

#include "run.h"
#include "parser/parser.h"
#include "models/memory.h"
#include "models/diagram.h"
#include "utils.h"
#include "view/menu.h"

#define DELAY 1000000

int runing = 1;

static turing_machine computer;
static int end;
static const char *filename_var;

pthread_t process_thread_id = 0;
pthread_t input_thread_id = 0;

/**
 * @brief init global variable of turing machine
 *
 */
void init_turing_machine(const char *filename)
{
    filename_var = filename;
    end = 0;

    setup_terminal();
    computer.mem = setup_memory();
    read_file(filename_var, &computer);
    finish_memory_setup(computer.mem);
}

static void proc_t_m_thread();

/**
 * @brief run turing_machine with input thread
 *
 */
void run_turing_machine()
{
    print_menu();
    pthread_create(&input_thread_id, NULL, &input, NULL);
    while (runing)
    {
        if (menu_state != BREAK)
        {
            proc_t_m_thread();
        }
        usleep(DELAY);
    }
    pthread_join(input_thread_id, NULL);
}

static void *process_turing_machine();

/**
 * @brief create thread for an iteration
 *  of the turing machine
 *
 */
static void proc_t_m_thread()
{
    if (!end)
    {
        if (process_thread_id != 0)
        {
            pthread_join(process_thread_id, NULL);
            process_thread_id = 0;
        }
        pthread_create(&process_thread_id, NULL, &process_turing_machine, NULL);
    }
}

static void apply_link(link_t *link);

/**
 * @brief makes an iteration of turing's machine
 *
 * @return void*
 */
static void *process_turing_machine()
{
    end = 1;
    link_t *tmp = computer.actual_node->links;
    while (tmp != NULL)
    {
        if (tmp->IN == computer.mem->band->character)
        {
            end = 0;
            apply_link(tmp);
            break;
        }
        tmp = tmp->next;
    }
}

/**
 * @brief apply the link to the compute
 *
 * @param link the link found
 */
static void apply_link(link_t *link)
{
    if (link->dir == LEFT)
    {
        go_left(link->OUT, computer.mem);
    }
    else
    {
        go_right(link->OUT, computer.mem);
    }
    computer.actual_node = link->node;
}

/**
 * @brief call when a button is used
 *
 * @param button
 * @return int
 */
int button_tap(int button)
{
    int ret = NOTHING;
    switch (button)
    {
    case QUIT:
        runing = 0;
        break;

    case RESET:
        init_turing_machine(filename_var);
        break;

    case RUN_BREAK:
        menu_state = (menu_state + 1) % 2;
        ret = CHANGE;
        break;

    case STEP:
        if (menu_state == BREAK)
        {
            proc_t_m_thread();
        }
    default:
        break;
    }
    return ret;
}

/**
 * @brief clear the memory
 *
 */
void free_turing_machine()
{
    free_memory(computer.mem);
    free_diagram(computer.diag);
    close_terminal();
}