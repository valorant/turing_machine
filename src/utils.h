#ifndef UTILS_H
#define UTILS_H

__attribute__((noreturn)) extern void error(char *output);
extern void setup_terminal();
extern void close_terminal();

#endif /* UTILS_H */