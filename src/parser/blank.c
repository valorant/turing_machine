#include "run.h"

#include "utils.h"
#include "parser/parser.h"

int blank_found = 0;

#define BLANK_VAR "blank"

int is_blank(turing_machine *computer)
{
    if (!check_keyword(BLANK_VAR))
    {
        return 0;
    }
    pass_point(BLANK_VAR);
    if (act_char != '\'')
    {
        sprintf(err, "error: input: expected ' (L : %d, C: %d)", line, col);
        error(err);
    }
    next_char();
    computer->mem->empty = act_char;
    next_char();
    if (act_char != '\'')
    {
        sprintf(err, "error: input: expected ' (L : %d, C: %d)", line, col);
        error(err);
    }
    blank_found = 1;
    return 1;
}