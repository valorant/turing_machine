#include "run.h"
#include "utils.h"
#include "parser/parser.h"

#include <stdlib.h>

#define NODE_VAR "nodes"

int node_found = 0;

static int get_number_of_nodes();
static void get_node_name(int number_of_node, turing_machine *computer);

int is_node(turing_machine *computer)
{
    if (!check_keyword(NODE_VAR))
    {
        return 0;
    }

    int nodes_number = get_number_of_nodes();
    if (nodes_number == 0)
    {
        sprintf(err, "error: no nodes\n");
        error(err);
        return 0;
    }

    get_node_name(nodes_number, computer);

    node_found = 1;
    return 1;
}

static int get_number_of_nodes()
{
    int number = 0;
    if (act_char != '[')
    {
        sprintf(err, "error: nodes '[' expected (L : %d, C: %d)", line, col);
        error(err);
    }
    else
    {
        next_char();
        pass_space();
        while (act_char != ']')
        {
            if (IS_DIGIT(act_char))
            {
                number *= 10;
                number += act_char - '0';
                next_char();
            }
            else if (IS_SPACE(act_char))
            {
                pass_space();
            }
            else
            {
                sprintf(err, "error: %c is not a number\n", act_char);
                error(err);
            }
        }
        next_char();
        pass_point(NODE_VAR);
    }
    return number;
}

static int i;

static void next_node();
static void read_node(char **nodes);

static void get_node_name(int number_of_node, turing_machine *computer)
{
    char **nodes = (char **)malloc(sizeof(char *) * number_of_node);
    if (act_char != '{')
    {
        sprintf(err, "error: nodes '{' expected (L : %d, C: %d)\n", line, col);
        error(err);
    }
    next_char();
    pass_space();
    i = 0;
    nodes[0] = (char *)malloc(sizeof(char) * NAME_LENGTH);
    int end = 1;
    while (end)
    {
        if (i >= number_of_node)
        {
            sprintf(err, "error: too many nodes (L : %d, C: %d)\n", line, col);
            error(err);
        }
        if (IS_LETTER(act_char))
        {
            read_node(nodes);
            if (act_char == ',')
            {
                next_node();
            }
            else if (i >= number_of_node - 1)
            {
                pass_space();
                if (act_char != '}')
                {
                    sprintf(err, "error: You missing '}' (L : %d, C: %d)\n", line, col);
                    error(err);
                }
                end = 0;
            }
            else
            {
                sprintf(err, "error: You missing ',' (L : %d, C: %d)\n", line, col);
                error(err);
            }
        }
        else if (IS_SPACE(act_char))
        {
            pass_space();
        }
        else
        {
            sprintf(err, "error: %c is not a letter\n", act_char);
            error(err);
        }
    }
    computer->diag = init_diagram((char **)nodes, i + 1);
    for (size_t j = 0; j < i; j++)
    {
        free(nodes[j]);
    }
    free(nodes);
}

static void read_node(char **nodes)
{
    int j = 0;
    nodes[i] = (char *)malloc(sizeof(char) * NAME_LENGTH);
    while (IS_LETTER(act_char))
    {
        nodes[i][j] = act_char;
        j++;
        next_char();
        if(j >= NAME_LENGTH)
        {
            sprintf(err, "error: %s is too long\n", nodes[i]);
            error(err);
        }
    }
    nodes[i][j] = '\0';
    pass_space();
}

static void next_node()
{
    i++;
    next_char();
    pass_space();
}