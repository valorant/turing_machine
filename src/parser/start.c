#include "run.h"
#include "utils.h"
#include "parser/parser.h"

#include <stdlib.h>

int start_found = 0;

#define START_VAR "start"

int is_start(turing_machine *computer)
{
    if (!check_keyword(START_VAR))
    {
        return 0;
    }

    pass_point(START_VAR);
    char start_name[NAME_LENGTH];
    if (act_char != '\'')
    {
        sprintf(err, "error: input: expected ' (L : %d, C: %d)", line, col);
        error(err);
    }
    int i = 0;
    next_char();
    while (act_char != '\'')
    {
        if (IS_LETTER(act_char))
        {
            start_name[i] = act_char;
            next_char();
            i++;
        }
    }
    start_name[i] = '\0';
    init_start(start_name, computer->diag);
    computer->actual_node = computer->diag->start;
    start_found = 1;
    return 1;
}