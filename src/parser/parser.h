#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>

#include "run.h"

#define IS_LETTER(c) ((c >= 'a' && c <= 'z') || \
                     (c >='A' && c <= 'Z'))
#define IS_COMMENT(c) (c == '#')
#define IS_SPACE(c) (c == ' ' || c == '\t' || c == '\n')
#define IS_DIGIT(c) (c >= '0' && c <= '9')
#define IS_ESCAPE(c) (c == '\\')

extern FILE *file;
extern char err[];
extern char act_char;
extern int line, col;

extern void read_file(const  char *filename, turing_machine *computer);
extern int check_keyword(const char * word);
extern void pass_point(const char * word);
extern void pass_space();
extern int next_char();
extern void escape_function();

#endif /* PARSER_H */