#include "utils.h"
#include "parser/parser.h"

#include <string.h>

#define LINK_VAR "link"

int link_found = 0;

int depth;

static void deth_controler(int depth, char *act_node, turing_machine *computer);

int is_link(turing_machine *computer)
{
    if (!check_keyword(LINK_VAR))
    {
        return 0;
    }
    pass_point(LINK_VAR);
    pass_space();
    depth = 0;
    char act_node[NAME_LENGTH];
    do
    {
        if (act_char == '}')
        {
            depth--;
            deth_controler(depth, act_node, computer);
            continue;
        }
        else if (act_char == '{')
        {
            depth++;
            deth_controler(depth, act_node, computer);
            continue;
        }
        next_char();
    } while (depth > 0);
    link_found = 1;
    return 1;
}

static void get_node(char *act_node);
static void link_analyse(char *act_node, turing_machine *computer);

static void deth_controler(int depth, char *act_node, turing_machine *computer)
{
    if (depth == 1)
    {
        get_node(act_node);
    }
    else if (depth == 3)
    {
        link_analyse(act_node, computer);
    }
    else
    {
        next_char();
        pass_space();
    }
}

static void get_node(char *act_node)
{
    next_char();
    pass_space();
    int i = 0;
    while (IS_LETTER(act_char))
    {
        act_node[i] = act_char;
        i++;
        next_char();
        if (i >= NAME_LENGTH)
        {
            sprintf(err, "error: %s is too long\n", act_node);
            error(err);
        }
    }
    act_node[i] = '\0';
}

static char get_char();
static enum direction get_dir();

static void link_analyse(char *act_node, turing_machine *computer)
{
    char IN,
        OUT = -1,
        next_node[NAME_LENGTH];
    strcpy(next_node, act_node);
    enum direction dir;
    next_char();
    IN = get_char();
    printf("%c ", IN);

    pass_space();
    dir = get_dir();
    printf("%d ", dir);
    next_char();
    pass_space();

    if (act_char != '}')
    {
        if (act_char == ':')
        {
            next_char();
            pass_space();
            if (act_char != '\'')
            {
                sprintf(err, "error: You missing \"'\" (L : %d, C: %d)\n", line, col);
                error(err);
            }
            get_node(next_node);
            printf("%c\n", act_char);
            if (act_char != '\'')
            {
                sprintf(err, "error: You missing \"'\" (L : %d, C: %d)\n", line, col);
                error(err);
            }
            next_char();
            pass_space();
        }
        if (act_char == 'w')
        {
            next_char();
            pass_space();
            pass_point(LINK_VAR);
            pass_space();
            OUT = get_char();
        }
    }
    putchar('\n');

    add_link(act_node, next_node, IN, OUT, dir, computer->diag);
}

static char get_char()
{
    pass_space();
    if (act_char != '\'')
    {
        goto error;
    }
    next_char();
    char ret = act_char;
    next_char();
    if (act_char != '\'')
    {
        goto error;
    }
    next_char();
    return ret;
error:
    sprintf(err, "error: You missing \"'\" (L : %d, C: %d)\n", line, col);
    error(err);
}

static enum direction get_dir()
{
    enum direction dir;
    switch (act_char)
    {
    case 'R':
        dir = RIGHT;
        break;

    case 'L':
        dir = LEFT;
        break;

    default:
        sprintf(err, "error: '%c' is not a direction (L : %d, C: %d)\n", act_char, line, col);
        error(err);
        break;
    }
    return dir;
}