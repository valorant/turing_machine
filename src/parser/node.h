#ifndef NODE_H
#define NODE_H

#include "run.h"

extern int node_found;

extern int is_node(turing_machine *computer);

#endif /* NODE_H */