#include <stdlib.h>
#include <stdio.h>

#include "run.h"
#include "utils.h"

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "./turing_machine <filename>\n");
        return 1;
    }
    init_turing_machine(argv[1]);

    run_turing_machine();

    free_turing_machine();

    return 0;
}
