#include <stdlib.h>

#include <ncurses.h>

#include "models/memory.h"
#include "view/view_mem.h"

static WINDOW *win;

static int center_x, win_width;
static int x_max, x_min;

static void print_case(char character, int x, int shift);
static void print_line(int shift);

/**
 * @brief data initialization for memory strip poster
 *
 */
void init_meme_view()
{
    win_width = ((COLS - 4) - (COLS - 4) % 4) + 1;
    win = subwin(stdscr, 6, win_width, LINES - 10, (COLS - win_width) / 2);
    int nb_case = (win_width - 5) / 4;
    x_min = -nb_case / 2;
    x_max = nb_case / 2 + (nb_case % 2);
}

/**
 * @brief memory display function
 *
 * @param memory
 * @param shift
 */
void print_memory(memory_t *memory, int shift)
{
    print_line(shift);
    if (memory->band != NULL)
    {
        band_t *tmp = memory->band->next;
        int i = 1;
        while (tmp != NULL && i < x_max)
        {
            print_case(tmp->character, i, shift);
            tmp = tmp->next;
            i++;
        }
        tmp = memory->band;
        i = 0;
        while (tmp != NULL && i >= x_min)
        {
            print_case(tmp->character, i, shift);
            tmp = tmp->prev;
            i--;
        }
    }
    wrefresh(win);
}

/**
 * @brief displays the empty memory strip
 *
 * @param shift
 */
static void print_line(int shift)
{
    center_x = win_width / 2 + win_width / 2 % 4;
    wclear(win);
    box(win, ACS_VLINE, ACS_HLINE);
    wmove(win, 1, center_x);
    wprintw(win, "*");
    int i;
    char line[3][6] = {"+---+", "+   +", "+---+"};
    for (i = shift - 6; i < win_width - 5; i += 4)
    {
        for (size_t j = 0; j < 3; j++)
        {
            if (i <= 0)
            {
                wmove(win, j + 2, 1);
                wprintw(win, line[j] + abs(i) + 1);
            }
            else
            {
                wmove(win, j + 2, i);
                wprintw(win, line[j]);
            }
        }
    }
    for (size_t j = 0; j < 3; j++)
    {
        line[j][win_width - i - 1] = 0;
        wmove(win, j + 2, i);
        wprintw(win, line[j]);
    }

    wrefresh(win);
}

/**
 * @brief print character in x case
 *
 * @param character
 * @param x
 * @param shift
 */
static void print_case(char character, int x, int shift)
{
    x = center_x + 4 * x + shift;
    wmove(win, 3, x);
    wprintw(win, "%c", character);
}

/**
 * @brief delete the window displaying the memory
 *
 */
void free_view_mem()
{
    delwin(win);
}