#ifndef MENU_H
#define MENU_H

enum menu_selected
{
    QUIT,
    RESET,
    RUN_BREAK,
    STEP
};

enum stat
{
    RUN,
    BREAK
};

enum menu_ret
{
    NOTHING,
    CHANGE
};

extern enum stat menu_state;

extern void *input();
extern void print_menu();

#endif /* MENU_H */