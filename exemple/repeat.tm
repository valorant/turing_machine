# Adds two binary numbers together.

blank: ' '

input: ' '

nodes [4] : {
  b,
  c,
  e,
  f
}

start: 'b'

link: {
  b
  {
    {' ' R: 'c' w: '0'}
  }
  c
  {
    {' ' R: 'e'}
  }
  e
  {
    {' ' R: 'f' w: '1'}
  }
  f
  {
    {' ' R: 'b'}
  }
}