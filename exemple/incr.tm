# Adds 1 to a binary number.

input: '1011'

blank: ' '

nodes [3] : {
  right,
  carry,
  done
}

start: 'right'

link :{
  right
  {
    {'1' R}
    {'0' R}
    {' ' L: 'carry'}
  }
  carry
  {
    {'1' L w: '0'}
    {'0' L: 'done' w: '1'}
    {' ' L: 'done' w: '1'}
  }
}